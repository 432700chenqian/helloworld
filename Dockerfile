FROM java:8
EXPOSE 8080
ADD /target/spring-boot-helloworld-0.0.1-SNAPSHOT.jar spring-boot-helloworld-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","spring-boot-helloworld-0.0.1-SNAPSHOT.jar"]